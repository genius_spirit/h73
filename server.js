const express = require("express");
const app = express();
const Vigenere = require('caesar-salad').Vigenere;
const password = 'qwerty';


app.get('/Hello', (req, res) => {
  res.send('Hello');
});

app.get('/encode/:word', (req, res) => {
  res.send(Vigenere.Cipher(password).crypt(`${req.params.word}`));
});

app.get('/decode/:word', (req, res) => {
  res.send(Vigenere.Decipher(password).crypt(`${req.params.word}`));
});

app.listen(8000);
